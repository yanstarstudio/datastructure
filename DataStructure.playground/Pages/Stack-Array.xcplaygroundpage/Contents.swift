// An implmentation of stack using array
import Foundation

struct Stack<Element> {

    private var array: [Element] = []

    mutating func push(_ element: Element) {
        array.append(element)
    }

    mutating func pop() -> Element? {
        return array.popLast()
    }

    func peek() -> Element? {
        return array.last
    }

    var isEmpty: Bool {
        return array.isEmpty
    }

    var count: Int {
        return array.count
    }
}

extension Stack: CustomStringConvertible {

    var description: String {

        let topDivider = "---Stack---\n"
        let bottomDivider = "\n-----------\n"

        let stackElements = array.map { "\($0)" }.reversed().joined(separator: "\n")
        return topDivider + stackElements + bottomDivider
    }
}


var movieStack = Stack<String>()
movieStack.push("Game over")
movieStack.push("american hustle")
print(movieStack)
movieStack.peek()

movieStack.pop()
movieStack.pop()


