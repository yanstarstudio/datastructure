import Foundation

public class Node<T> {

    public var value: T
    public var next: Node<T>?
    public weak var previous: Node<T>?

    public init(value: T) {
        self.value = value
    }
}

public class LinkedList<T> {

    private var head: Node<T>?
    private var tail: Node<T>?
    public init() {}

    public var isEmpty: Bool {
        return head == nil
    }

    public var first: Node<T>? {
        return head
    }

    public var last: Node<T>? {
        return tail
    }

    public func append(value: T) {
        let newNode = Node(value: value)

        if let tailNode = tail {
            newNode.previous = tailNode
            tailNode.next = newNode
        } else {
            head = newNode
        }
        tail = newNode
    }

    public func node(at index: Int) -> Node<T>? {
        guard index >= 0 else { return nil }
        var node = head
        var i = index

        while node != nil {
            if i == 0 { return node }
            i -= 1
            node = node!.next
        }
        return node
    }

    public func removeAll() {
        head = nil
        tail = nil
    }

    public func remove(node: Node<T>) -> T {
        let prev = node.previous
        let next = node.next

        if let prev = prev {
            prev.next = next
        } else {
            head = next
        }
        next?.previous = prev

        if next == nil {
            tail = prev
        }

        node.previous = nil
        node.next = nil

        return node.value
    }

    public var count: Int {
        guard head != nil else { return 0 }
        var currentHead = head
        var count = 0
        while currentHead != nil {
            count += 1
            currentHead = currentHead?.next
        }
        return count
    }

}

extension LinkedList: CustomStringConvertible {
    public var description: String {
        var text = "["
        var node = head

        while node != nil {
            text += "\(node!.value)"
            node = node!.next
            if node != nil { text += ", "}
        }
        return text + "]"
    }
}


