import Foundation

public struct Queue<T> {

    private var list = LinkedList<T>()

    public init() {}
    
    public mutating func enqueue(_ element: T) {
        list.append(value: element)
    }

    public mutating func dequeue() -> T? {
        guard !list.isEmpty, let elemnt = list.first else { return nil }
        list.remove(node: elemnt)
        return elemnt.value
    }

    public func peek() -> T? {
        return list.first?.value
    }

    public var isEmpty: Bool {
        return list.isEmpty
    }
}
