import Foundation

public enum EdgeType {
    case directed, undirected
}

public struct Edge<T: Hashable>: Hashable {
    public var source: Vertex<T>
    public var destination: Vertex<T>
    public let weight: Double?

    public var hashValue: Int {
        return "\(source)\(destination)\(String(describing: weight))".hashValue
    }

    public init(source: Vertex<T>, destination: Vertex<T>, weight: Double?) {
        self.source = source
        self.destination = destination
        self.weight = weight
    }
}

