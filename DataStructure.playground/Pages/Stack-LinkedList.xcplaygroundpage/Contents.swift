// An implmentation of stack using LinkedList

import Foundation

struct StackLinked<T> {

    private var list = LinkedList<T>()

    mutating func push(_ element: T) {
        list.append(value: element)
    }

    mutating func pop() -> T? {
        guard let lastNode = list.last else { return nil }
        return list.remove(node: lastNode)
    }

    func peek() -> T? {
        return list.last?.value
    }

    var isEmpty: Bool {
        return list.isEmpty
    }

    var count: Int {
        return list.count
    }
}

var stack = StackLinked<Int>()
stack.push(1)
stack.push(2)
stack.push(3)
print(stack)

stack.peek()

stack.pop()
print(stack)


