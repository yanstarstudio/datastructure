import Foundation

/// Vertex
public struct Vertex<T: Hashable>: Hashable, CustomStringConvertible {
    var data: T

    public var description: String {
        return "\(data)"
    }

    public init(data: T) {
        self.data = data
    }
}

